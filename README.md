# SocketProgramming

# Description
This program is a simple client/server application based on data (in JSON format) transfer with sockets.

Program demonstration:

[![VIDEO](https://img.youtube.com/vi/9v4JPzvzREI/0.jpg)](https://www.youtube.com/watch?v=9v4JPzvzREI)



## Commands
* ```uptime``` - returns server lifetime.
* ```info``` - returns server's version and creation date.
* ```help``` - returns list of possible commands with short description.
* ```stop``` - stops server and client running.
